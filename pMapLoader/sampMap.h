#ifndef SAMP_MAP_H_INCLUDED
#define SAMP_MAP_H_INCLUDED

#include <string>

#include <sampgdk/a_players.h>
#include <sampgdk/a_samp.h>
#include <sampgdk/core.h>
#include <sampgdk/sdk.h>
#include <sampgdk/interop.h>

namespace pMapLoader
{
	class SampMap
	{
	public:
		SampMap();
		SampMap(std::string fileName);

		~SampMap();

		bool Load();
		bool Load(std::string fileName);
	};
}

#endif