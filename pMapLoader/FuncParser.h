#ifndef FUNCPARSER_H_INCLUDED
#define FUNCPARSER_H_INCLUDED

#include <string>
#include <sstream>
#include <vector>

#include <boost\tokenizer.hpp>

namespace pMapLoader
{
	/*
		Parse something like : FunctionName(params, params, params); (parameters are exemple)
	*/

	class FuncParser
	{
	public:
		FuncParser();
		~FuncParser();

		void parse(std::string line);

		std::string getFuncName();

		int getNbrParams();
		std::string getNextParam();

		void clear();
	
	private:

		std::string m_funcName;

		std::string m_line;
		std::vector<std::string> m_params;
	};
}

#endif 